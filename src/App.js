import React, { Component } from 'react';
import './App.css';
import Request from 'superagent';

class App extends Component {
 constructor(props) {
    super(props);
    this.state = {
      body: "",
      value: "",
      name: "",
      abilities: "",
      order: "",
      weight: "",
      height: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value.toLowerCase()});
  }

  handleSubmit(event) {
    var url = "https://pokeapi.co/api/v2/pokemon/"+this.state.value;
    Request.get(url).then((response) => {
      this.setState({
        body: response.body,
        height: response.body.height,
        weight: response.body.weight,
        abilities: response.body.abilities,
        name: response.body.name,
        order: response.body.order,
        picFront: response.body.sprites.front_default,
        picBack: response.body.sprites.back_default,
        picShiny: response.body.sprites.front_shiny,


      });
    });
    event.preventDefault();
  }

  render() {

    return (
      <div className="flex">
          <div className="App">
          <h1>Search Pokemon</h1>
          <form onSubmit={this.handleSubmit}>
              <input type="text" value={this.state.value} onChange={this.handleChange} />
              <input type="submit" value="Submit" />
          </form>
        </div>
        <div className="app2">
          <h1><small>{this.state.order} </small>{this.state.name}</h1>
          <img alt={this.state.name} src={this.state.picFront}/>
          <img alt={this.state.name} src={this.state.picBack}/>
          <img alt={this.state.name} src={this.state.picShiny}/>
          <p>Height: {this.state.height}</p>
          <p>Weight: {this.state.weight}</p>
          <p>list of abilities here</p>
        </div>
      </div>

    );
  }
};


export default App;